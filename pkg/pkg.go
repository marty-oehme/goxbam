package pkg

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"regexp"
	"sort"
	"strconv"
	"strings"

	"github.com/spf13/viper"
)

type Package string

const (
	Template  Package = "templates"
	Processor Package = "processors"
)

func Get(ptype Package) []string {
	// set regexes for /base16 or /baseproc16
	var regex string
	if ptype == Template {
		regex = `^.+/base16-[[:alnum:]]*$`
	} else if ptype == Processor {
		regex = `^.+/baseproc16-[[:alnum:]]*$`
	}

	// get the directories
	templdir := fmt.Sprintf("%s/%s/", viper.GetString("ContentDir"), ptype)
	dirs := getUniqueDirsToDepth(templdir, 1)

	// make sure they comply with the base16 naming scheme
	valid := make([]string, 0)
	naming := regexp.MustCompile(regex)
	for _, v := range dirs {
		contains := naming.FindString(v)
		if contains != "" {
			valid = append(valid, v)
		}
	}

	return valid
}

func Themes() []string {
	templdir := fmt.Sprintf("%s/%s/", viper.GetString("ContentDir"), Template)
	themeset := make(map[string]bool, 0)

	regex := regexp.MustCompile(`^base16-([0-9A-Za-z_-]+)\.?.+$`)
	for _, v := range getAllFiles(templdir, -1) {
		v := filepath.Base(v)
		sub := regex.FindStringSubmatch(v)
		if len(sub) == 0 {
			continue
		}
		if v != "" {
			themeset[sub[1]] = true
		}
	}
	themefiles := make([]string, 0)
	for k, _ := range themeset {
		themefiles = append(themefiles, k)
	}
	sort.Strings(themefiles)
	return themefiles
}

// recurse into dir to find unique folders for the packages
// dir is the root search directory
// depth is how many levels should be *appended*, i.e.
// 0 is only this directory, 1 is one directory down, 2 twice, etc.
func getUniqueDirsToDepth(dir string, depth int) []string {
	// filter everything after the second slash
	maxdepth := regexp.MustCompile(`[^/]*(/[^/]*){` + strconv.Itoa(depth) + `}`)

	// make it a set, so every path only gets added once
	validset := make(map[string]bool)

	err := filepath.Walk(dir,
		func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}
			// when we find another directory node
			if info.IsDir() == true {
				// remove the base path we started from
				path = strings.Replace(path, dir, "", 1)

				// remove everything beyond max depth
				if depth > -1 {
					path = maxdepth.FindString(path)
				}

				// add it to our set
				validset[path] = true
			}
			return nil
		})
	if os.IsNotExist(err) {
		err = ensureDirExist(dir)
	}
	if err != nil {
		panic(err)
	}

	// copy keys into a string slice
	packages := make([]string, 0)
	for k := range validset {
		if k == "" {
			continue
		}
		packages = append(packages, k)
	}

	return packages
}

// Get only the directories within target directory
func filterDirs(dir string) ([]string, error) {
	dirs := make([]string, 0)
	files, err := ioutil.ReadDir(dir)
	if os.IsNotExist(err) {
		err = ensureDirExist(dir)
		if err != nil {
			return nil, err
		}
	}

	for _, v := range files {
		if v.IsDir() == true {
			dirs = append(dirs, v.Name())
		}
	}
	return dirs, nil
}

// Make sure target directory exists
func ensureDirExist(dir string) error {
	if _, err := os.Stat(dir); os.IsNotExist(err) {
		err = os.MkdirAll(dir, 0755)
		if err != nil {
			return err
		}
	}
	return nil
}

// recurse into dir to find unique folders for the packages
// dir is the root search directory
// depth is how many levels should be *appended*, i.e.
// 0 is only this directory, 1 is one directory down, 2 twice, etc.
func getAllFiles(dir string, depth int) []string {
	// filter everything after the x-th slash
	maxdepth := regexp.MustCompile(`[^/]*(/[^/]*){` + strconv.Itoa(depth) + `}`)

	found := make([]string, 0)

	err := filepath.Walk(dir,
		func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}
			// when we find another directory node
			if info.IsDir() == false {
				// remove the base path we started from
				path = strings.Replace(path, dir, "", 1)

				// remove everything beyond max depth
				if depth > -1 {
					path = maxdepth.FindString(path)
				}

				// add it to our set
				found = append(found, path)
			} else {
				dir := filepath.Base(path)
				if ".git" == dir {
					return filepath.SkipDir
				}
			}
			return nil
		})
	if os.IsNotExist(err) {
		err = ensureDirExist(dir)
	}
	if err != nil {
		panic(err)
	}

	return found
}
