package pkg

import (
	"errors"
	"fmt"
	"log"
	"os/exec"
	"regexp"

	"github.com/spf13/viper"
)

func Download(args []string) {
	for _, v := range args {

		p, err := getPackageType(v)
		if err != nil {
			fmt.Printf("%s %s\n", v, err.Error())
			continue
		}

		shortlink, link := getShortandLongLink(v)

		// attempt git clone
		path := fmt.Sprintf("%s/%s/%s", viper.GetString("ContentDir"), p, shortlink)
		err = exec.Command("git", "clone", link, path).Run()
		if err != nil {
			log.Printf("Command finished with error: %v", err)
			panic(err.Error())
		}
		log.Printf("Cloned %s: %s", p, shortlink)

		//// retry git clone for github-style link -- with gitlab

		// exit successfully or throw error

	}
}

// check type (template/processor) or return error
func getPackageType(arg string) (Package, error) {
	// look for /base16- somewhere in there
	if regexp.MustCompile(`.+/base16-.+`).FindString(arg) != "" {
		return Template, nil
		// look for /baseproc16- somewhere in there
	} else if regexp.MustCompile(`.+/baseproc16-.+`).FindString(arg) != "" {
		return Processor, nil
	} else {
		return Processor, errors.New("does not signify a valid package: make sure it is a template or processor")
	}
}

func getShortandLongLink(link string) (string, string) {
	var shortlink, longlink string
	// check github-style or direct-link
	if isShortlink(link) == true {
		shortlink = link
		longlink = makeGithublink(link)
	} else {
		var err error
		shortlink, err = makeShortlink(link)
		if err != nil {
			fmt.Printf("malformed link: %s can not get author/repository", link)
			return "", ""
		}
		longlink = link
	}
	return shortlink, longlink
}

func isShortlink(link string) bool {
	if regexp.MustCompile(`^[0-9A-Za-z-_]+/{1}[0-9A-Za-z-_]+$`).FindString(link) != "" {
		return true
	}
	return false
}

func makeGithublink(short string) string {
	return fmt.Sprintf("https://github.com/%s", short)
}

func makeShortlink(long string) (string, error) {
	// extracts username/repository from a link like https://github.com/marty-oehme/baseproc16-vim -> marty-oehme/baseproc16-vim
	subs := regexp.MustCompile(`^.+[/:]{1}([0-9A-Za-z_-]+/base(?:proc)?16-[0-9A-Za-z_-]+)(?:\..+)?$`).FindStringSubmatch(long)
	if len(subs) != 2 {
		return "", errors.New("malformed link, can not form short link")
	}
	return subs[1], nil
}
