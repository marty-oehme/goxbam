/*
Copyright © 2020 Marty Oehme

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/marty-oehme/goxbam/pkg"
)

// downloadCmd represents the download command
var downloadCmd = &cobra.Command{
	Use:   "download [package] [package] [...]",
	Short: "Installs packages into its data directory.",
	Args:  cobra.MinimumNArgs(1),
	Long: `The download command takes any number of links or github-
style repository strings and will download all of them into
its data directory ready for use. 

The downloader automatically detects whether the downloaded 
package is a base16 template or a baseproc16 processor, and
organizes it correctly. 

Download links can point to any valid git repository 
directly, or take the form user/repository. If an input has
this form the downloader will automatically try to download
from the corresponding github repository, or, if this fails
will attempt to download from the corresponding gitlab
directory. If both fail it will warn the user. 

Examples: 

goxbam download https://gitlab.com/marty-oehme/goxbam.git

goxbam download git@gitlab.com/marty-oehme/goxbam.git

goxbam download marty-oehme/goxbam

`,
	Run: func(cmd *cobra.Command, args []string) {
		pkg.Download(args)
	},
}

func init() {
	rootCmd.AddCommand(downloadCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// downloadCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// downloadCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
